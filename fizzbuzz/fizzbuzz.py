import unittest

# FizzBuzz -> https://en.wikipedia.org/wiki/Fizz_buzz
# The exercise is complete when the following input:
# ```
# [1, 2, 3, 5, 6, 10, 15, 30]
# ```
# results in the following output:
# ```
# "1, 2, Fizz, Buzz, Fizz, Buzz, FizzBuzz, FizzBuzz"
# ```

def fizz_buzz(number):
    return FizzBuzz.execute(number)

class FizzBuzz():

    def __init__(self, fizz_mult=3, buzz_mult=5):
        self._fizz_mult = fizz_mult
        self._buzz_mult = buzz_mult


    def calculate(self, number):
        isDivisibleBy3 = FizzBuzz.isDivisibleBy(self._fizz_mult, number)
        isDivisibleBy5 = FizzBuzz.isDivisibleBy(self._buzz_mult, number)

        ret = ''
        if isDivisibleBy3:
            ret = 'Fizz'
        if isDivisibleBy5:
            ret += 'Buzz'

        if isDivisibleBy3 or isDivisibleBy5:
            return ret
        else:
            return str(number)

    @staticmethod
    def isDivisibleBy(x, number):
        return number % x == 0

    @staticmethod
    def execute(number):
        return FizzBuzz().calculate(number)



class TestFizzBuzz(unittest.TestCase):
    def test_one(self):
        self.assertEqual(fizz_buzz(1), '1')

    def test_two(self):
        self.assertEqual(fizz_buzz(2), '2')

    def test_first_fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')


    def test_first_fizz_on_class(self):
        self.assertEqual(FizzBuzz.execute(3), 'Fizz')

    def test_four(self):
        self.assertEqual(fizz_buzz(4), '4')

    def test_first_buzz(self):
        self.assertEqual(fizz_buzz(5), 'Buzz')

    def test_second_fizz(self):
        self.assertEqual(fizz_buzz(6), 'Fizz')

    def test_second_buzz(self):
        self.assertEqual(fizz_buzz(10), 'Buzz')

    def test_first_fizzbuzz(self):
        self.assertEqual(fizz_buzz(15), 'FizzBuzz')

    def test_class_execution(self):
        self.assertEqual(FizzBuzz().calculate(15), 'FizzBuzz')

    def test_custom_multiplier(self):
        fizz_buzz = FizzBuzz(2,5)
        self.assertEqual(fizz_buzz.calculate(10), 'FizzBuzz')

    # def test_upper(self):
    #     self.assertEqual('foo'.upper(), 'FOO')
    #
    # def test_isupper(self):
    #     self.assertTrue('FOO'.isupper())
    #     self.assertFalse('Foo'.isupper())
    #
    # def test_split(self):
    #     s = 'hello world'
    #     self.assertEqual(s.split(), ['hello', 'world'])
    #     # check that s.split fails when the separator is not a string
    #     with self.assertRaises(TypeError):
    #         s.sp_Flit(2)

if __name__ == '__main__':
    unittest.main()
