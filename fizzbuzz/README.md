# FizzBuzz -> https://en.wikipedia.org/wiki/Fizz_buzz

The exercise is complete when the following input:

```
[1, 2, 3, 5, 6, 10, 15, 30]
```

results in the following output:

```
"1, 2, Fizz, Buzz, Fizz, Buzz, FizzBuzz, FizzBuzz"
```
