const score = require('./score');
const Game = score.Game;

test('adds 1 + 2 to equal 3', () => {
  expect(score.sum(1, 2)).toBe(3);
});


test('Intial score is love-all', () => {
  expect(new  Game('p1', 'p2').getScore()).toBe('love all');
});
